from time import sleep

from selenium.webdriver.common.by import By
from unittest import TestCase
from selenium import webdriver


class Functionaltest(TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_title(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertIn('Busco Ayuda', self.browser.title)


    def test_registro(self):
        self.browser.get('http://127.0.0.1:8000/')
        link = self.browser.find_element_by_id('id_registro')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombres')
        nombre.send_keys('Pedro')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Casas')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('pedro@email.com')

        experiencia = self.browser.find_element_by_id('id_experiencia')
        experiencia.send_keys('10')

        self.browser.find_element_by_xpath("//select[@id='id_servicio']/option[text()='Ingeniero']").click()

        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('3006785432')

        imagen = self.browser.find_element_by_id('id_foto')
        imagen.send_keys("d:\\temp\\img.jpg")

        password = self.browser.find_element_by_id('id_password')
        password.send_keys('pedro123')

        password = self.browser.find_element_by_id('id_password_confirm')
        password.send_keys('pedro123')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()

        self.browser.implicitly_wait(3)
        span = self.browser.find_element(By.XPATH, '//span[text()="Pedro Casas"]')
        self.assertIn('Pedro Casas', span.text)

    def test_verdetalle(self):
        self.browser.get('http://127.0.0.1:8000/')
        span = self.browser.find_element(By.XPATH, '//div/span[text()="Pedro Casas"]/../img')
        span.click()
        sleep(0.5)

        h3 = self.browser.find_element(By.XPATH, '//h3[text()="Pedro Casas"]')
        self.assertIn('Pedro Casas', h3.text)

    def test_registro_iniciosesion(self):
        self.browser.get('http://127.0.0.1:8000/')
        link = self.browser.find_element_by_id('id_login')
        link.click()

        self.browser.implicitly_wait(5)

        correo = self.browser.find_element_by_id('username')
        correo.send_keys('pedro@email.com')

        password = self.browser.find_element_by_id('password')
        password.send_keys('pedro123')

        botonLogin = self.browser.find_element_by_id('id_loginBoton')
        botonLogin.click()

        self.browser.implicitly_wait(10)
        h3 = self.browser.find_element(By.XPATH, '//h3[text()="Bienvenido"]')
        self.assertIn('Bienvenido', h3.text)

    def test_xedicion_independiente(self):
        self.browser.get('http://127.0.0.1:8000/')
        linkl = self.browser.find_element_by_id('id_login')
        linkl.click()

        self.browser.implicitly_wait(5)

        correol = self.browser.find_element_by_id('username')
        correol.send_keys('pedro@email.com')

        passwordl = self.browser.find_element_by_id('password')
        passwordl.send_keys('pedro123')

        botonLogin = self.browser.find_element_by_id('id_loginBoton')
        botonLogin.click()

        self.browser.implicitly_wait(7)

        link = self.browser.find_element_by_id('mi_cuenta')
        link.click()

        self.browser.implicitly_wait(7)

        nombre = self.browser.find_element_by_id('id_form-0-nombres')
        nombre.send_keys('Pedro P')

        apellidos = self.browser.find_element_by_id('id_form-0-apellidos')
        apellidos.send_keys('Casas C')

        experiencia = self.browser.find_element_by_id('id_form-0-experiencia')
        experiencia.send_keys('20')

        self.browser.find_element_by_xpath("//select[@id='id_form-0-servicio']/option[text()='Plomeria']").click()

        telefono = self.browser.find_element_by_id('id_form-0-telefono')
        telefono.send_keys('3001234567')

        submit = self.browser.find_element_by_id('editBtn')
        submit.click()

        self.browser.implicitly_wait(7)

        h1 = self.browser.find_element(By.XPATH, '//h1[text()="BUSCO AYUDA"]')
        self.assertIn('BUSCO AYUDA', h1.text)

    def test_registro_adicioncomentario(self):
        self.browser.get('http://127.0.0.1:8000/')
        span = self.browser.find_element(By.XPATH, '//div/span[text()="Pedro Casas"]/../img')
        span.click()
        sleep(1)

        correol = self.browser.find_element_by_id('id_email_Pedro_Casas')
        correol.send_keys('comentador@email.com')

        passwordl = self.browser.find_element_by_id('id_comentario_Pedro_Casas')
        passwordl.send_keys('mi comentario')

        submit = self.browser.find_element_by_id('id_btnguardar_Pedro_Casas')
        submit.click()
        sleep(0.5)

        alert = self.browser.switch_to_alert()
        alert_text = alert.text
        self.assertIn('Comentario Creado', alert_text)

    def test_ybusqueda_independiente(self):
        self.browser.get('http://127.0.0.1:8000/')
        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('pedro@email.com')

        botonBuscar = self.browser.find_element_by_id('id_filtrarBoton')
        botonBuscar.click()

        self.browser.implicitly_wait(5)

        span = self.browser.find_element(By.XPATH, '//div/span/../img')
        span.click()
        self.browser.implicitly_wait(5)

        h3 = self.browser.find_element(By.XPATH, '//h3[text()="PedroPedro P CasasCasas C"]')
        self.assertIn('PedroPedro P CasasCasas C', h3.text)